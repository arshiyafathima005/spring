package com.org.spring;

public class BookTicket {
	private String ticketNumber;
	private String from;
	private String to;
	private PassengerDetails  details;
	
	
public BookTicket() {
	System.out.println(this.getClass().getSimpleName() +" object created");
}
public void booked()
{
	System.out.println("Ticket Number : "+ this.ticketNumber +" from : "+ this.from +" to :"+ this.to +" has booked successfully!" );	
}


public String getTicketNumber() {
	return ticketNumber;
}

public void setTicketNumber(String ticketNumber) {
	this.ticketNumber = ticketNumber;
}

public String getFrom() {
	return from;
}

public void setFrom(String from) {
	this.from = from;
}

public String getTo() {
	return to;
}

public void setTo(String to) {
	this.to = to;
}

public PassengerDetails getDetails() {
	return details;
}
public void setDetails(PassengerDetails details) {
	this.details = details;
}
@Override
public String toString() {
	return "BookTicket [ticketNumber=" + ticketNumber + ", from=" + from + ", to=" + to + ", details=" + details + "]";
}

}