package com.org.spring;

public class PassengerDetails {
	private String name;
	private String age;
	
	public PassengerDetails(String name, String age) {
		super();
		this.name = name;
		this.age = age;
	}
   
	@Override
	public String toString() {
		return "PassengerDetails [name=" + name + ", age=" + age + "]";
	}
	

}
